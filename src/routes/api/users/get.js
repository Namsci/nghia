var mongoose = require('mongoose');
var returnToUser = require('services/returnToUser');


module.exports = router => {
  router.get('/get', (req, res) => {
        mongoose.model('users').find((err, result) => {
          if (err) return returnToUser.errorProcess(res, err);
          if (result) {
            return returnToUser.success(res, "Get new user success", result)
          } else {
            return returnToUser.errorWithMess(res, "Add failed")
          }
        })
      })
};
