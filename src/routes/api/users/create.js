var mongoose = require('mongoose');
var returnToUser = require('services/returnToUser');
var bcryptjs = require('bcryptjs');
const { check, validationResult } = require("express-validator/check");
var { initializeUserFolder } = require('services/createFolder');

module.exports = router => {
  var checkInput = [
    check('name').not().isEmpty(),
    check('userName').not().isEmpty(),
    check('password').not().isEmpty(),
    check('email').not().isEmpty(),
  ]
  router.post('/create-user', checkInput, (req, res) => {
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      bcryptjs.hash(req.body.password, 10, (err, hashPassword) => {
        if (err) throw err;
        if (hashPassword) {
          let insert = {
            name: req.body.name,
            userName: req.body.userName,
            email: req.body.email,
            password: hashPassword,
          }
          mongoose.model('users').create(insert, (err, result) => {
            if (err) throw err;
            if (result) {
              initializeUserFolder(result._id, (err) => {
                if (err) throw err;
              });
              return returnToUser.success(res, "Add new user success", result)
            } else {
              return returnToUser.errorWithMess(res, "Add failed")
            }
          })
        }
      })
    } catch (error) {
      if (error) return returnToUser.errorProcess(res, err);
    }

  })
};
