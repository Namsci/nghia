var express = require('express');
var router = express.Router();

require("./create")(router);
require("./get")(router);

module.exports = router;