var router = require('express').Router();

router.use('/datas', require('./datas'));
router.use('/users', require('./users'));


module.exports = router;