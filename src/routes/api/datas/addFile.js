var { screenshots, logs } = require('services/uploadFile');
var mongoose = require('mongoose');
var returnToUser = require('services/returnToUser');
const { check, validationResult } = require("express-validator/check");
var bcryptjs = require('bcryptjs');
var config = require('config')
module.exports = router => {
    router.post('/:userId/add-log', [logs().single('log')], async (req, res, next) => {
        try {
            mongoose.model('users').findById(req.params.userId, async (err, result) => {
                if (err) throw err;
                let compare = await bcryptjs.compare(req.body.password, result.password);
                if (compare == true) {
                    let filter = {
                        userId: result._id,
                        isCurrent: true
                    }
                    let update = {
                        logs: {
                            $push: {
                                filename: req.file.filename
                            }
                        }
                    }
                    mongoose.model('datas').findOneAndUpdate(filter, update, (err, result) => {
                        if (err) throw err;
                        return returnToUser.successWithNoData(res, "Done")
                    })
                }
            });
        } catch (err) {
            if (err) return returnToUser.errorProcess(res, err);
        }
    })
    router.post('/:userID/add-screenshot', [screenshots().single('screenshot')], async (req, res, next) => {
        try {
            mongoose.model('users').findById(req.params.userID, async (err, result) => {
                if (err) throw err;
                let compare = await bcryptjs.compare(req.body.password, result.password);
                if (compare == true) {
                    let filter = {
                        userId: result._id,
                        isCurrent: true
                    }
                    let update = {
                        screenShots: {
                            $push: {
                                filename: req.file.filename
                            }
                        }
                    }
                    mongoose.model('datas').findOneAndUpdate(filter, update, (err, result) => {
                        if (err) throw err;
                        return returnToUser.success(res, "Done")
                    })
                }
            });
        } catch (err) {
            if (err) return returnToUser.errorProcess(res, err);
        }
    })
}