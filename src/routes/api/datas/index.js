var express = require('express');
var router = express.Router();

require("./addFile")(router);
require("./getAllFileSendEmail")(router);
require("./addNewDataDocument")(router);

module.exports = router;