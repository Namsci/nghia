var mongoose = require('mongoose');
var returnToUser = require('services/returnToUser');
var {initializeDataFolder} = require('services/createFolder');

module.exports = router => {
  router.post('/add-data-document', (req, res) => {
    try {
      mongoose.model('users').find((err, result) => {
        if (err) throw err;
        result.map((item) =>{
          let insert = {
            userId: item._id,
            date: new Date().getTime(),
            isCurrent: true
          }
          mongoose.model('datas').create(insert, (err, result) => {
              if (err) throw err;
              initializeDataFolder(result.userId, (err)=>{
                if (err) throw err;
              })
            })
        })
      })
      return returnToUser.success(res, 'Add data slots success')
    } catch (err) {
      return returnToUser.errorWithMess(res, "Add failed" + err)
    }
  })
};
