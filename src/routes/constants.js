module.exports = {
  EXISTS: { $exists: true, $ne: null },
  DATA_TYPE: ['REGISTER', 'RECEIVED_LOG']
};