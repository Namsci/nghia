var path = require("path");
var fs = require("fs");
var { google } = require("googleapis");
var drive = google.drive("v3");
var key = require("../config").googleVerify;
var config = require("../config");
var mime = require("mime-types");

var jwToken = new google.auth.JWT(
  key.client_email,
  null,
  key.private_key,
  ["https://www.googleapis.com/auth/drive"],
  null
);

jwToken.authorize(authErr => {
  if (authErr) {
    console.log("error : " + authErr);
    return;
  } else {
    console.log("Authorization accorded");
  }
});

module.exports = {
  uploadFileToDrive: (fileLink) => {
    return new Promise((resolve, reject) => {
      let folderId = config.googleFolderId;   // Folder private
      fs.exists(fileLink, exists => {
        if (exists) {
          let fileMetadata = {
            name: path.basename(fileLink),
            parents: [folderId]
          };
          let media = {
            mimeType: mime.lookup(fileLink),
            body: fs.createReadStream(fileLink)
          };
  
          drive.files.create(
            {
              auth: jwToken,
              resource: fileMetadata,
              media: media,
              fields: "id"
            },
            function(err, file) {
              if (err) return reject(err)
              if (file) {
                fs.unlink(fileLink, err => {
                  if (err) {
                    return reject(err)
                  } else {
                    return resolve(file.data);
                  }
                });
              } else {
                return reject(false);
              }
            }
          );
        } else {
          return reject(false);
        }
      });
    })
  },

  downloadFileFromDrive: (googleFileId, title) => {
    return new Promise((resolve, reject) => {
      drive.files.get(
        {
          auth: jwToken,
          fileId: googleFileId,
          alt: "media"
        }, 
        { responseType: "stream" },
        function(err, res) {
          if (err) return reject(err);
          res.data
            .on("end", () => {
              return resolve(`src/private/documents/${title}`);
            })
            .on("error", err => {
              return reject(err);
            })
            .pipe(fs.createWriteStream(`src/private/documents/${title}`));
        }
      );
    })
  },

  viewFileFromDrive: (googleFileId, title = null) => {
    return new Promise((resolve, reject) => {

      let folder_private = config.googleFolderId;
      let folder_public = config.googleFolderId_public;
      drive.files.copy(Object.assign({}, {
        auth: jwToken,
        fileId: googleFileId,
      }, title ? {resource: { 'name' : title }} : {}), function (err, file) {
        if (err) {
          reject(err);
        } else {
          let newFileId = file.data.id;
          drive.files.update({
            auth: jwToken,
            fileId: newFileId,
            addParents: [folder_public],
            removeParents: [folder_private],
            fields: 'id, parents'
          }, function (err, file2) {
            if (err) {
              reject(err);
            } else {
              //------------ 
              // Lay link de VIEW = <"https://drive.google.com/open?id=" + newFileId>
              //------------
              setTimeout(function() {
                drive.files.delete({
                  auth: jwToken,
                  fileId: newFileId,
                  parents: [folder_public],
                }, function (err, file3) {
                  if (err) {
                    // Handle error
                    console.log('Loi khi delete dinh ky', err);
                  } else {
                    // Move the file to the new folder
                    console.log('Delete file theo dinh ky thanh cong id = ' + newFileId);
                  }
                });
              }, 1000 * 60 * 30);
              resolve(newFileId)
            }
          });
        }
      });
    })


  }
}