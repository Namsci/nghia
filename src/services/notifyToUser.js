var mongoose = require('mongoose');
var slugify = require('slugify');
var _ = require('lodash');

findUsersDetail = async (userId) => {
  try {
    return await mongoose.model('users').findById(userId, '_id name')
  } catch (err) {
    throw new Error(err)
  }
}

module.exports = {
  createNotify: async (fromUser, toUser, body, isSentFromSystem = false) => {
    let toUserInfo = await findUsersDetail(toUser);
    let fromUserInfo = isSentFromSystem ? { name: "Hệ thống", _id: null } : await findUsersDetail(fromUser);

    if (_.isEmpty(toUserInfo) || _.isEmpty(fromUserInfo)) return false;

    let insert = {
      from: fromUserInfo._id,
      fromName: fromUserInfo.name,
      to: toUserInfo._id,
      toName: toUserInfo.name,
      title: _.get(body, 'title', ''),
      slug: `${slugify(_.get(body, 'title', ''))}-${new Date().getTime()}`,
      content: _.get(body, 'content', ''),
      link: _.get(body, 'link', '')
    }

    return await mongoose.model('notify').create(insert);
  }
}