var fs = require('fs');
function createFolder(path, mask, cb){
    if (typeof mask == 'function') {
        cb = mask;
        mask = 0777;
    }
    fs.mkdir(path, mask, function (err) {
        if (err) {
            if (err.code == 'EEXIST') cb(null);
            else cb(err);
        } else cb(null);
    });
}
module.exports = {
    createFolder,
    initializeUserFolder: (userId, cb) => {
        try {
            var date = new Date().getDate();
            createFolder(`src/data/${userId}`, 0744, (err) => {
                if (err) throw err
                createFolder(`src/data/${userId}/${date}`, 0744, (err) => {
                    if (err) throw err
                    createFolder(`src/data/${userId}/${date}/Keylogs`, 0744, (err) => {
                        if (err) throw err
                    })
                    createFolder(`src/data/${userId}/${date}/Screenshots`, 0744, (err) => {
                        if (err) throw err
                    })
                })
            })
            cb(null);
        } catch (err) {
            cb(err);
        }
    },
    initializeDataFolder: (userId, cb) => {
        try {
            var date = new Date().getDate();
            createFolder(`src/data/${userId}/${date}`, 0744, (err) => {
                if (err) throw err
                createFolder(`src/data/${userId}/${date}/Keylogs`, 0744, (err) => {
                    if (err) throw err
                })
                createFolder(`src/data/${userId}/${date}/Screenshots`, 0744, (err) => {
                    if (err) throw err
                })
            })
            cb(null);
        } catch (err) {
            cb(err);
        }
    }
}