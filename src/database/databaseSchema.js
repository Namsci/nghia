var mongoose = require('mongoose');
var schema = require('./schema');

module.exports = {
  users: mongoose.model('users', schema.users),
  data: mongoose.model('datas', schema.datas),
}