var mongoose = require('mongoose');
var constants = require('../../routes/constants');

var users = new mongoose.Schema({
  name: {
    required: true,
    type: String
  },
  userName: {
    required: true,
    type: String
  },
  email: {
    required: true,
    type: String
  },
  password: {
    required: true,
    type: String
  },
  dataByDate:[{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'datas'
  }]
})

module.exports = users;