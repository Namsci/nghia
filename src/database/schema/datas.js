var mongoose = require('mongoose');

var datas = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'users'
  },
  date: {
    type: Date,
    default: new Date(),
    required: true
  },
  isCurrent: {
    type: Boolean,
    default: true,
    required: true
  },
  logs: [{
    filename:{
      type: String
    }
  }],
  screenShots: [{
    filename:{
      type: String
    }
  }],
})

module.exports = datas;