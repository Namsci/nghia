var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var engine = require('ejs-locals');
var config = require("config/index");

require("database/connectDatabase");
require("database/databaseSchema");

var app = express();
app.set("topSecretKey", config.serectKey);

// view engine setup
app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
// Adding cache css, js to improve page load

var whitelist = ["http://localhost:3000"];

app.use(express.static(path.join(__dirname, 'src', 'data'), { maxAge: '30 days' }));

//Adding checking Authenticaed
require("routes/checkAuthenticated")(app);
// // Adding routes
require("routes/index")(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(false);
});

module.exports = app;
